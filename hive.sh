#!/bin/bash

echo "*************************************************************************************************"
echo "                                     HIVE TEST"
echo "**************************************************************************************************"
sleep 2
echo -e
#Moving Data Files to tmp folder to enable access to hdfs user
cp /root/sampledata.txt /tmp
cp /root/partitiondata.txt /tmp

sudo -su hdfs hive -e "set hive.cli.print.current.db=true;"

echo -e
echo " HDFS path for Hive Metastore to create User-defined Db/Tables/Data"
sudo -su hdfs hive -e "set hive.metastore.warehouse.dir;"
sleep 5
echo -e
echo "************************************************************************
      	1) Creating User-Defined Database - testdb
        2) Creating Table Employee with Array,Map,Struct type Fields
      	3) Loading & Querying for Data in Employee Table
      	4) Creating Customer Table with partitions Country & State
      	5) Loading & Querying for Data in Customer Table"
echo "************************************************************************"
echo -e
sleep 3

sudo -su hdfs hive -e "
CREATE DATABASE IF NOT EXISTS testdb;
DESCRIBE DATABASE testdb;
USE testdb;
CREATE TABLE IF NOT EXISTS testdb.employee_test(name STRING  COMMENT'Employee Name',
                                               salary FLOAT COMMENT'Employee Salary',
                                               subordinates ARRAY<STRING> COMMENT'Subordinate Details',
                                               deductions MAP<STRING,FLOAT> COMMENT'Salary Deductions',
                                               address STRUCT<street:STRING,
                                                               city:STRING,
                                                               zip:INT> COMMENT'Employee Home Address'
                                                )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
COLLECTION ITEMS TERMINATED BY ':'
MAP KEYS TERMINATED BY '-'
LINES TERMINATED BY '\n'
STORED AS TEXTFILE;
LOAD DATA LOCAL INPATH '/tmp/sampledata.txt' OVERWRITE INTO TABLE employee_test;
SELECT * FROM employee_test;
CREATE TABLE IF NOT EXISTS testdb.customer_test(id INT COMMENT'Customer Id',
                                                name STRING COMMENT'Customer Name',
                                                company STRING COMMENT 'Company Name',
                                                income FLOAT COMMENT'Customer Income',
                                                privilegestatus STRING COMMENT'Privileged Customer'
                                               )
PARTITIONED BY(country STRING,state STRING)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
STORED AS TEXTFILE;
LOAD DATA LOCAL INPATH '/tmp/partitiondata.txt' OVERWRITE INTO TABLE customer_test PARTITION(country='India',state='Karnataka');
LOAD DATA LOCAL INPATH '/tmp/partitiondata.txt' OVERWRITE INTO TABLE customer_test PARTITION(country='India',state='Maharashtra');
LOAD DATA LOCAL INPATH '/tmp/partitiondata.txt' OVERWRITE INTO TABLE customer_test PARTITION(country='China',state='Sichuan');
LOAD DATA LOCAL INPATH '/tmp/partitiondata.txt' OVERWRITE INTO TABLE customer_test PARTITION(country='US',state='California');
LOAD DATA LOCAL INPATH '/tmp/partitiondata.txt' OVERWRITE INTO TABLE customer_test PARTITION(country='US',state='Georgia');
SELECT * FROM customer_test;
"
sleep 5
echo -e
echo " Listing Equivalent HDFS Directory tree for virtual Hive DB Tables along with branches for the Partition Data"
echo -e
hadoop fs -ls -R /user/hive/warehouse
sleep 5
echo -e
echo "Dropping DB & Tables"
sudo -su hdfs hive -e "
DROP TABLE testdb.employee_test;
DROP TABLE testdb.customer_test;
DROP DATABASE testdb;
"
exit