#!/bin/bash

echo "***********************************************************************************"
echo                             "HDFS & MapReduce Testing"
echo "***********************************************************************************"

echo "Changing to tmp folder"
cd /tmp

echo "Creating wordcount class folder for .class files"
mkdir wordcount_classes

echo "Creating input files for WordCount MapReduce job"
echo "Hello World Bye World" > file0
echo "Hello Hadoop Goodbye Hadoop" > file1

#Giving full permissions to the created files & folders
chmod 777 file0
chmod 777 file1
chmod 777 wordcount_classes

echo "Listing the created and folders along with their permissions"
ls -l

echo "***********************************************************************************"
echo                             "Starting HDFS/MR test"
echo "***********************************************************************************"

echo "Creating Input Directory in HDFS"
sudo -su hdfs hadoop fs -mkdir /user/qa/wordcount/input

echo "Copying file* from tmp directory to hdfs directory"
sudo -su hdfs hadoop fs -put file* /user/qa/wordcount/input

echo "Listing the file paths & directories created in HDFS"
echo "User should be able to the file replicas in this listing"
sudo -su hdfs hadoop fs -ls -R /user

# Exporting Java path for running wordcount.java
export JAVA_HOME=/usr/java/jdk1.6.0_31
export PATH=$PATH:$JAVA_HOME/bin

#Compiling wordcount.java
javac -cp /usr/lib/hadoop/*:/usr/lib/hadoop/client-0.20/* -d wordcount_classes /root/WordCount.java

#Creating jar file for wordcount.java
jar -cvf wordcount.jar -C wordcount_classes/ .
chmod 777 wordcount.jar

echo "Running the wordcount.jar on input files & seeing the output"
sudo -su hdfs hadoop jar ./wordcount.jar org.myorg.WordCount /user/qa/wordcount/input /user/qa/wordcount/output
sudo -su hdfs hadoop fs -ls /user/qa/wordcount/output 
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00000
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00001
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00002
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00003
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00004
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00005
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00006
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00007
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00008
sudo -su hdfs hadoop fs -cat /user/qa/wordcount/output/part-00009

echo "Deleting files and directories in HDFS path"
sudo -su hdfs hadoop fs -rm -r -skipTrash /user/qa/wordcount/output
sudo -su hdfs hadoop fs -rm -r -skipTrash /user/qa/wordcount/input
sudo -su hdfs hadoop fs -rm -r -skipTrash /user

echo "Trying to list HDFRS path that doesnt exist any more"
sudo -su hdfs hadoop fs -ls -R /user

echo "Cleaning the tmp folder (to enable clean reruns of the script)"
rm -r wordcount_classes
rm wordcount.jar
rm file*

echo "***********************************************************************************"
echo                             "Ending HDFS/MR test"
echo "***********************************************************************************"

exit